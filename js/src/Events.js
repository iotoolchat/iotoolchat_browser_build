XmppChat.Events = (function(xc) {
    'use strict';

    var elapsed_seconds = 0;
    var call_timer;

    xc.onDisconnect = function() {
	$('#rooms_area').empty();
    };

    xc.capabilityUpdate = function(jid, c) {
	var id = xc.Utils.jidToId(jid);
	
	console.log(jid, 'has following capabilities:', c);

	var device = c.client_type;
	var webcam = _.find(c.extended_service.x.data, {v: 'webcam'}).data.value;
	var mic = _.find(c.extended_service.x.data, {v: 'microphone'}).data.value;
	var webrtc = _.find(c.extended_service.x.data, {v: 'webrtc'}).data.value;

	var ind = '';

	if (device === 'pc') {
	    ind += '<i class="fa fa-desktop" aria-hidden="true"></i>';
	} else {
	    ind += '<i class="fa fa-mobile" aria-hidden="true"></i>';
	}

	if (webcam) {
	    ind += '<i class="fa fa-video-camera" aria-hidden="true"></i>';
	} else {
	    ind += '<i class="fa fa-video-camera" aria-hidden="true" style="color: #cecece;"></i>';
	}

	if (mic) {
	    ind += '<i class="fa fa-microphone" aria-hidden="true"></i>';
	} else {
	    ind += '<i class="fa fa-microphone" aria-hidden="true" style="color: #cecece;"></i>';
	}

	if (webrtc) {
	    ind += '<i class="fa fa-check" aria-hidden="true"></i>';
	} else {
	    ind += '<i class="fa fa-check" aria-hidden="true" style="color: #cecece;"></i>';
	}
	
	xc.UI.setContactMediaInfo(id, ind);
    };

    xc.call = {
	ENDED: 0,
	STARTED: 1
    };

    xc.event = {
	PAGE_CHANGE: 0
    };

    xc.notifyCallWatcher = function(call_status, data) {
	var id = xc.Utils.jidToId(data.jid);
	var file;
	
	switch (call_status) {
	case xc.call.STARTED:
	    console.debug('Call started', data);
	    // start call timer
	    call_timer = setInterval(function() {
		elapsed_seconds++;
	    }, 1000);


	    file = 'IoToolChat/' + id + '.note';
	    // save what user has typed in so far
	    xc.Utils.editorSaveFile(file, function() {
		xc.Utils.addTimestampToQuillFile(file, data.type, data.status, null, function() {
		    xc.Utils.loadUsersLog(xc.editor, id, id);
		});
	    });


	    break;
	case xc.call.ENDED:
	    console.debug('Call ended', data);
	    // end call timer
	    clearInterval(call_timer);

	    file = 'IoToolChat/' + id + '.note';
	    // save what user has typed in so far
	    xc.Utils.editorSaveFile(file, function() {
		xc.Utils.addTimestampToQuillFile(file, data.type, data.status, elapsed_seconds, function() {
		    xc.Utils.loadUsersLog(xc.editor, id, id);
		});
	    });

	    xc.UI.editorVideoHang.remove();
	    xc.UI.adjustEditorDimensions();

	    break;
	default:
	    break;
	}
    };

    function handlePageChange(page, event) {
	console.log(page);
	switch(page) {
	case 'login_page':
	    if (xc.Utils.userConnected()) {
		xc.navigate_back = false;
	    } else {
		xc.navigate_back = true;
	    }
	    break;
	default:
	    break;
	}
    }
    
    // Main event watcher
    xc.mainWatcher = function(event_type, data, event) {
	if (event_type === xc.event.PAGE_CHANGE) {
	    handlePageChange(data.toPage[0].id, event);

	    // TODO: Define rules for each page


	    // Login page rules:
	    // * Strophe must be disconnected if is connected
	    // move to contacts
	    // * WebRTC must be inactive (file transfer and media)
	    // * If user is already connected move to contacts

	    // Contacts, chat, and settings page rules:
	    // * User must be connected to the xmpp if not clean
	    // any bosh sessions and move to login screen
	    // * WebRTC must be inactive

	    // Chat page rules:
	    // * User must be connected to xmpp if not move to login
	    // and clean any connectons
	    // * Only WebRTC file transfer can be active audio and
	    // video must be inactive

	    // Incoming video call rules:
	    // * WebRTC must be inactive and user must be connected
	    // to the bosh server if not move to login page
	    // * Accept and decline buttons must be visible

	    // Outgoing video call page:
	    // * WebRTC must be inactive

	    // Video call page:
	    // * WebRTC must be active, if after set amount of
	    // time webrtc session is not live or there are any errors
	    // move to previous page and display error message to the
	    // user

	    // Incoming audio call page:
	    // * WebRTC must not be active if audio call was not accepted
	    // by both peers
	    // * accept and decline buttons must be visible if call is not accepted
	    // yet
	    // * If webrtc connection fails display an error message
	    // * user must be connected to the xmpp server
	    // * call timer must be stopped and zeroed
	    // * notes button should not be visible

	    // Outgoing audio call page
	    // * WebRTC can be active only after call was accepted
	    // user must be connected to the xmpp server

	    // notes page rules
	    // call can be active but hangup button must be present
	    // user must be connected to the server
	   
	    console.info('Main watcher page change:', data);
	}
    };

    return xc;
})(XmppChat || {});
